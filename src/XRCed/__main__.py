
import sys

try:
    from XRCed.xrced import main
except ImportError:
    print('XRCed parent directory must be in PYTHONPATH for local running', file=sys.stderr)
    raise

main()
